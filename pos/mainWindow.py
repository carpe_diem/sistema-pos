# -*- coding: ISO-8859-1 -*-
import wx
import ConfigParser
import users
from sales import NewSaleWindow, SaleDetailWindow, SaleDetailUserWindow
from products import AddProductWindow, EditProductWindow 
from about import AboutDialogBox

ID_EXIT = 101
ID_SALE = 201
ID_ADD_PROD = 202
ID_DEL_PROD = 203
ID_EDIT_PROD = 204
ID_SALE_DET = 205
ID_SALE_USER_DET = 206
ID_ADD_USER = 301
ID_PREF = 302
ID_HELP = 401
ID_ABOUT = 402


cfgFile = '/usr/local/pos/config.cfg'
config = ConfigParser.SafeConfigParser()
resultLst = config.read(cfgFile)

icon_size = config.get('Settings','icon_size')
title = config.get('Settings','title')


class MainFrame(wx.MDIParentFrame):
    
    def __init__(self, options, pos, size):
        wx.MDIParentFrame.__init__(self, None, -1, title, pos, size)
        self.options = options
        icondir = self.options['icondir']
        
        menuBar = wx.MenuBar()

        menuArchivo = wx.Menu()
        menuBar.Append(menuArchivo, "&Archivo")
		
        quit = wx.MenuItem(menuArchivo, ID_EXIT, "&Salir\tCtrl+W", "Salir de la aplicacion")
        quit.SetBitmap(wx.Bitmap(icondir+'/16x16/exit.png'))
        menuArchivo.AppendItem(quit)

        menuSale = wx.Menu()
        menuBar.Append(menuSale, "&Ventas")
        newSale = wx.MenuItem(menuSale, ID_SALE, "Nueva venta\tCtrl+N", "Realizar nueva venta")
        newSale.SetBitmap(wx.Bitmap(icondir+'/16x16/sale.png'))
        menuSale.AppendItem(newSale)
        saleDetUser = wx.MenuItem(menuSale, ID_SALE_USER_DET, "Mis ventas",
            "Detalle de ventas por usuario")
        saleDetUser.SetBitmap(wx.Bitmap(icondir+'/16x16/details.png'))
        menuSale.AppendItem(saleDetUser)

        if options['admin'] == 1:
            saleDet = wx.MenuItem(menuSale, ID_SALE_DET, "Detalle de ventas", "Detalle de ventas")
            saleDet.SetBitmap(wx.Bitmap(icondir+'/16x16/details.png'))
            menuSale.AppendItem(saleDet)

            product = wx.Menu()
        
            newTicket = wx.MenuItem(product, ID_ADD_PROD, "Nueva entrada", "Agregar una aueva entrada")
            newTicket.SetBitmap(wx.Bitmap(icondir+'/16x16/add.png'))
            product.AppendItem(newTicket)

            editTicket = wx.MenuItem(product, ID_EDIT_PROD, "Modificar precio", "Modificar precio de las entradas")
            editTicket.SetBitmap(wx.Bitmap(icondir+'/16x16/edit.png'))
            product.AppendItem(editTicket)
		
        #~ product.Append(ID_DEL_PROD, "Borrar Producto", "Borrar un producto")
            menuSale.AppendMenu(-1, 'Entradas', product)

        if options['admin'] == 1:
            #Menu User
            menuUser = wx.Menu()
            menuBar.Append(menuUser, "&Sistema")
            newUser = wx.MenuItem(menuUser, ID_ADD_USER, "Usuarios", "Abre la pantalla para el manejo de usuarios")
            newUser.SetBitmap(wx.Bitmap(icondir+'/16x16/user.png'))
            menuUser.AppendItem(newUser)
        
            #~ menuUser.Append(ID_PREF, "Preferencias", "Abre la pantalla de preferencias")
		
        #Menu Ayuda
        menuHelp = wx.Menu()
        menuBar.Append(menuHelp, "Ay&uda")
        #~ menuHelp.Append(ID_HELP, "Ayuda...", "Ayuda")
        #~ menuHelp.AppendSeparator()
        about = wx.MenuItem(menuHelp, ID_ABOUT, "Acerca de...\tF1", "Informacion de la aplicacion")
        about.SetBitmap(wx.Bitmap(icondir+'/16x16/about.png'))
        menuHelp.AppendItem(about)
		
		#Creo el toolbar
        toolbar = self.CreateToolBar()
        toolbar.AddSimpleTool(ID_EXIT, wx.Bitmap(icondir + icon_size + '/exit.png'), 'Salir', 'Salir de la aplicacion')
        toolbar.AddSeparator()
        toolbar.AddSimpleTool(ID_SALE, wx.Bitmap(icondir + icon_size + '/sale.png'), 'Nueva venta', 'Realizar nueva venta')
        toolbar.AddSeparator()
 
		
        if options['admin'] == 1:
            toolbar.AddSimpleTool(ID_ADD_USER, wx.Bitmap(icondir + icon_size + '/user.png'), 'Nuevo usuario', 'Agregar nuevo usuario')
            toolbar.AddSimpleTool(ID_ADD_PROD , wx.Bitmap(icondir + icon_size + '/add.png'), 'Agregar Entrada', 'Agregar nuevo ticket')
            toolbar.AddSimpleTool(ID_EDIT_PROD , wx.Bitmap(icondir + icon_size + '/edit.png'), 'Editar Entrada', 'Editar precio de entradas')
            toolbar.AddSimpleTool(ID_SALE_DET , wx.Bitmap(icondir + icon_size + '/details.png'), 'Detalle de ventas', 'Detalle de ventas')
								
        toolbar.Realize()
		              
        #Crear la barra de Menues y de Status
        self.SetMenuBar(menuBar)
        #~ self.statusbar = self.CreateStatusBar()
        #~ self.SetStatusText("Elija alguna opcion del menu para comenzar")
        self.StatusBar()
        self.statusbar.SetStatusText('Elija alguna opcion del menu para comenzar', 0)
        self.statusbar.SetStatusText('Usuario: ' + options['username'], 1)
		        
        #Bindeo de funciones
        self.Bind(wx.EVT_MENU, self.OnExit, id=ID_EXIT)
        self.Bind(wx.EVT_MENU, self.OnNewSale, id=ID_SALE)
        self.Bind(wx.EVT_MENU, self.OnAddProduct, id=ID_ADD_PROD)
        self.Bind(wx.EVT_MENU, self.OnEditProduct, id=ID_EDIT_PROD)
        self.Bind(wx.EVT_MENU, self.OnSaleDetail, id=ID_SALE_DET)
        self.Bind(wx.EVT_MENU, self.OnSaleUserDetail, id=ID_SALE_USER_DET)
        self.Bind(wx.EVT_MENU, self.OnAddUser, id=ID_ADD_USER)
        self.Bind(wx.EVT_MENU, self.OnAbout, id=ID_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnPreferences, id=ID_PREF)
 	
    def StatusBar(self):
        self.statusbar = self.CreateStatusBar()
        self.statusbar.SetFieldsCount(3)
        self.statusbar.SetStatusWidths([-5, -2, -1])
		
    def OnExit(self, evt):
        dlg = wx.MessageDialog(self, 'Va a salir del sistema POS, esta seguro?', 'Confirmacion', wx.YES_NO | 
		wx.NO_DEFAULT | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)
        
    # Ventas
    def OnNewSale(self, evt):
        newSaleWindow = NewSaleWindow(self, "Ventas", self.options['username'], (1,1) , (1,1))
        newSaleWindow.Show(True)

    def OnAddProduct(self, evt):
        self.SetStatusText("Complete todos los datos para agregar una nueva entrada")
        addProductWindow = AddProductWindow(self, "Nueva Entradas", (1,1) , (1,1))
        addProductWindow.Show(True)

    def OnEditProduct(self, evt): 
        self.SetStatusText("Seleccione el precio que desea cambiar")
        editProductWindow= EditProductWindow(self, "Editar Entradas", (1,1) , (1,1))
        editProductWindow.Show(True)

    def OnSaleDetail(self, evt):
        saleDetailWindow = SaleDetailWindow(self, "Detalle ventas", (1,1) , (1,1))
        saleDetailWindow.Show(True)

    def OnSaleUserDetail(self, evt):
        user = self.options['username']
        saleDetailUserWindow = SaleDetailUserWindow(self, user,
            "Detalle ventas diario por usuario", (1,1) , (1,1))
        saleDetailUserWindow.Show(True)


    # Sistema
    def OnAddUser(self, evt):
        self.SetStatusText("Complete todos los datos para agregar nuevo Usuario")
        addUserWindow = users.AddUserWindow(self, "Usuarios", (1,1) , (1,1))
        addUserWindow.Show(True)

    def OnPreferences(self, evt):
        preferencesWindow = PreferencesWindow(self, "Preferencias", (1,1) , (1,1))
        preferencesWindow.Show(True)

    # Acerca de...
    def OnAbout(self, evt):
        aboutWindow = AboutDialogBox(self, None, "Acerca de")

