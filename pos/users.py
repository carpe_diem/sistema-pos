# -*- coding: ISO-8859-1 -*-
import  wx
from dbDriver import Users

   
ID_CLOSE = 102
ID_SAVE = 103

class AddUserWindow(wx.MDIChildFrame):
    def __init__(self, parent, title, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)

        panel = wx.Panel(self)

        topLbl = wx.StaticText(panel, -1, "Agregar nuevo Usuario al Sistema")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        
        first_nameLbl = wx.StaticText(panel, -1, "Nombre:")
        self.first_name = wx.TextCtrl(panel, -1, "", size=(250,-1));
        self.first_name.SetFocus()
        
        last_nameLbl = wx.StaticText(panel, -1, "Apellido:")
        self.last_name = wx.TextCtrl(panel, -1, "", size=(250,-1));
        
        usernameLbl = wx.StaticText(panel, -1, "Usuario:")
        self.username = wx.TextCtrl(panel, -1, "", size=(150,-1));
        
        passwdLbl = wx.StaticText(panel, -1, "Password:")
        self.passwd = wx.TextCtrl(panel, -1, "", size=(150,-1), style=wx.TE_PASSWORD);

        adminLbl = wx.StaticText(panel, -1, "Administrador")
        self.admin = wx.CheckBox(panel, -1 , "", (15, 55))

        saveBtn = wx.Button(panel, ID_SAVE, "Guardar")
        cancelBtn = wx.Button(panel, ID_CLOSE, "Cancelar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addrSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(first_nameLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.first_name, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        
        addrSizer.Add(last_nameLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.last_name, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        addrSizer.Add(usernameLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.username, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        addrSizer.Add(passwdLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.passwd, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        addrSizer.Add(adminLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.admin, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        
        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(saveBtn)

        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        mainSizer.Add(btnSizer, 0, wx.RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)
        
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnSaveWindow, id=ID_SAVE)
        
    def OnCloseWindow(self, evt):
        self.Close()

    def OnSaveWindow(self, evt):
        #~ SetStatusText("Crear nuevo usuario")
        data = {}
        data['first_name'] = self.first_name.GetValue()
        data['last_name'] = self.last_name.GetValue()
        data['username'] = self.username.GetValue()
        data['passwd'] = self.passwd.GetValue()
        data['admin'] = int(self.admin.GetValue())

        if Users().createUser(data) is None:
            dlg = wx.MessageDialog(None, 'El usuario %s ya existe' % data['username'], 'Usuarios', wx.OK | 
                wx.ICON_EXCLAMATION)
            dlg.ShowModal()
        else:
            self.OnCloseWindow(evt)
