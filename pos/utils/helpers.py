# -*- coding: ISO-8859-1 -*-
import datetime


def dateDelta(startDate, years=None, months=None, days=None):
    ret = startDate
    if years:
        ret = safeMakeDate(ret.year + years, ret.month, ret.day)
    if months:
        monthSum = ret.month + months
        newMonth = (monthSum - 1) % 12 + 1
        newYear = ret.year + (monthSum - 1) // 12
        ret = safeMakeDate(newYear, newMonth, ret.day)
    if days:
        ret += timedelta(days)
    return ret


