# -*- coding: ISO-8859-1 -*-
import os
import re
import ConfigParser
import sqlite

# compatibilidad con python <= 2.4
try:
    import sqlite3 as sqlite
except:
    from pysqlite2 import dbapi2 as sqlite


SqlScript = '''
create table user(
  user_id INTEGER PRIMARY KEY,
  username TEXT,
  first_name TEXT,
  last_name TEXT,
  password TEXT,
  admin  BOOLEAN default 0
);

create table ticket(
  ticket_id INTEGER PRIMARY KEY,
  description TEXT,
  price REAL,
  date_from TEXT,
  date_to TEXT
);

create table sale(
	sale_id INTEGER PRIMARY KEY,
	date text,
	user_id INTEGER,
    valid INTEGER,
	foreign key (user_id) references user(user_id)
);

create table sale_ticket(
	ticket_id INTEGER,
	sale_id INTEGER,
	cant integer,
	PRIMARY KEY(ticket_id, sale_id)
	foreign key (ticket_id) references ticket(ticket_id)
	foreign key (sale_id) references sale(sale_id)
);
 
create table version(
  version INTEGER
);

insert into user(user_id, first_name, last_name, username, password, admin)
    values( 0, "admin", "admin", "admin", "admin", 1);
insert into version values("1.01");

'''
cfgFile = '/usr/local/pos/config.cfg'
config = ConfigParser.SafeConfigParser()
resultLst = config.read(cfgFile)


class driverSqlite:

    def connection(self):
		obj = sqlite.connect(config.get('Settings','dbfile'))
		cur = obj.cursor()
		return cur, obj
	
    def create_database(self):
		cur = self.connection()
		cur.executescript(SqlScript)

# TODO
# Pener esto en script de instalacion		
# driverSqlite().create_database()


########## Users SQL class ###########

class Users:

    def __init__(self):
        self.cur, self.obj = driverSqlite().connection()

    def login(self, data):

        self.cur.execute("""SELECT username, password , admin
							FROM user 
							WHERE username = '%(IdUsername)s'
                            AND password = '%(IdPasswd)s' """ % {
                            	'IdUsername': data['username'], 
								'IdPasswd':data['passwd']
								})
							
        return self.cur.fetchone()

    def createUser(self, data=None):
        
        if self.findUsername(data['username']) is None:
            	
            self.cur.execute("""INSERT INTO user (first_name, last_name, username, password, admin)
								VALUES ("%(Idfirst_name)s", "%(Idlast_name)s", "%(Idusername)s",
										 "%(Idpasswd)s", "%(Idadmin)s")""" % {
											'Idfirst_name': data['first_name'],
											'Idlast_name': data['last_name'],
											'Idusername': data['username'],
											'Idpasswd': data['passwd'],
											'Idadmin': data['admin']
											})
            self.obj.commit()
            return data

        else:
            return None
	
    def findUsername(self, data):
        self.cur.execute("""SELECT username 
							FROM user
							WHERE username = "%s" """ % data)
							
        return self.cur.fetchone()

    def findUsernameId(self, data):

        self.cur.execute("""SELECT user_id
							FROM user
							WHERE username = "%s" """ % data)

        return self.cur.fetchone()
		
########## Tickets SQL class ###########

class Tickets:
	
    def __init__(self):
        self.cur, self.obj = driverSqlite().connection()

    def findTicket(self):
		
        fields = ['description', 'price']
		
        self.cur.execute("""SELECT ticket_id, description, price 
							FROM ticket
							WHERE date_to is null""")
        result = self.cur.fetchall()							

        c = []		
        for i in range(len(result)):
            dict = {}
            dict['id'] = result[i][0]
            dict['description'] = result[i][1]
            dict['price'] = result[i][2]
            c.append(dict)

        return c

    def insertTicket(self, data):

        self.cur.execute("""INSERT INTO ticket 
                            VALUES(null, '%s', %s, datetime("now","localtime"), null)""" % 
                            #~ (str(data[0]), decimal.Decimal(data[1], 2))
                            (str(data[0]), data[1])
                        )
        self.obj.commit()
        
    def updateTicket(self, data):
        """
            Este metodo actualiza el precio de una entrada.
            Para esto le pone fecha de fin a la entrada vigente y agrega una nueva con el nuevo precio. 
        """        
        self.cur.execute("""SELECT description
                            FROM ticket 
                            WHERE ticket_id = %s""" % data[1]
                        )                
        description = self.cur.fetchone()

        self.cur.execute("""UPDATE ticket
                            SET date_to = datetime("now","localtime")
                            WHERE ticket_id = %s""" %  data[1]
                        )
        self.obj.commit()        

        self.cur.execute("""INSERT INTO ticket(ticket_id, description, price, date_from, date_to)
                            VALUES(null, '%s', %s, datetime("now","localtime"), null) """ % 
                            (str(description[0]), str(data[0]))
                        )
        self.obj.commit()


########## Sales SQL class ###########
			
class Sale:

    def __init__(self):
        self.cur, self.obj = driverSqlite().connection()

    def get_next_id(self):
        """ Return the next id """
        self.cur.execute("SELECT MAX(sale_id) FROM sale")
        max = self.cur.fetchone()
        max_id = max[0] + 1
        return max_id

    def newSale(self, data, username):
        """
            data:
            {0: [u'1', u'2']}
            ie: {0: ['ticket_id', 'cantidad'}
        """
        #~ username = 1
        username = Users().findUsernameId(username)
        if not username:
            username = 1
        
        self.cur.execute("""INSERT INTO sale VALUES(null, datetime("now","localtime"), %s, 0)""" % (username))
        self.obj.commit()

        self.cur.execute("SELECT MAX(sale_id) FROM sale")
        max = self.cur.fetchone()
        max_id = max[0]
        
        for i in range(len(data)):
            ticket_id = data[i][0]
            cantidad =  data[i][1]

            self.cur.execute("""INSERT INTO sale_ticket
                                VALUES(%s, %s, %s)""" %  
                                (ticket_id , max_id, data[i][1]))
            self.obj.commit()


    def billing_user(self, date=None, user=None):
        """ Retunr billing per day by user """

        filters = ''
        filters += " WHERE sale.valid = 0 "
        filters += " AND user.username = '%s' " % (user)
        if date is not None:
            dateFrom = str(date) + ' 00:00:00'
            dateTo = str(date) + ' 23:59:59'
            filters += " AND sale.date between '%s' and '%s' " % (dateFrom, dateTo)

        sql = """SELECT ticket.description as Entrada, 
                        SUM(sale_ticket.cant) as Cantidad,
                        ticket.price as Precio, 
                        SUM(ticket.price * sale_ticket.cant) as Total,
                        user.username as Usuario
                 FROM user join sale 
                    ON user.user_id = sale.user_id 
                 JOIN sale_ticket
                    ON sale.sale_id = sale_ticket.sale_id 
                 JOIN ticket 
                    ON sale_ticket.ticket_id = ticket.ticket_id"""

        filters += " GROUP BY ticket.description,ticket.price, user.username "
        query = sql + filters

        self.cur.execute(query)
        result = self.cur.fetchall()

        c = []		
        for i in range(len(result)):
            dict = {}
            dict['description'] = result[i][0]
            dict['cant'] = result[i][1]
            dict['price'] = result[i][2]
            dict['total'] = result[i][3]
            dict['user'] = result[i][4]
            dict['date'] = date
            c.append(dict)

        return c


    def selectAll(self, dateRange=None, month=None, user=None):
        """
            @month = ['01-12-2009 00:00:00', '01-01-2010 00:00:00']
        """

        filters = ''
        if dateRange[0] is not None:
            dateFrom = str(dateRange[0]) + ' 00:00:00'
        else: 
            dateFrom = dateRange[0]
        if dateRange[1] is not None:
            dateTo = str(dateRange[1]) + ' 23:59:59'
        else: 
            dateTo = dateRange[1]

        if dateFrom and dateTo:
            filters = " WHERE sale.date between '%s' and '%s' " % (dateFrom, dateTo)
        elif dateFrom and not dateTo:
            filters = " WHERE sale.date between '%s' and datetime('now','localtime') " % (dateFrom)
        elif not dateFrom and dateTo:
            filters = " WHERE sale.date between '2009-01-01' and '%s' " % (dateTo)

        sql = """SELECT sale.sale_id as Numero,
                        ticket.description as Entrada, 
                        ticket.price as Precio, 
                        sale_ticket.cant as Cantidad,
                        ticket.price * sale_ticket.cant as Total,
                        CASE sale.valid 
                            WHEN  0 THEN 'NO'
                            ELSE 'SI'
                        END as Anulado,
                        sale.date as Fecha_Hora,
                        user.username as Usuario
                 FROM user join sale 
                    ON user.user_id = sale.user_id 
                 JOIN sale_ticket
                    ON sale.sale_id = sale_ticket.sale_id 
                 JOIN ticket 
                    ON sale_ticket.ticket_id = ticket.ticket_id"""
        
        filters += " ORDER by sale.date "
        query = sql + filters

        self.cur.execute(query)
        result = self.cur.fetchall()							

        c = []		
        for i in range(len(result)):
            dict = {}
            dict['id'] = result[i][0]
            dict['description'] = result[i][1]
            dict['price'] = result[i][2]
            dict['cant'] = result[i][3]
            dict['total'] = result[i][4]
            dict['anulado'] = result[i][5]
            dict['datetime'] = result[i][6]
            dict['username'] = result[i][7]
            c.append(dict)

        return c
    
    def cancelSale(self, data):
        """
            Anulada: valid = 1
        """
        self.cur.execute("""UPDATE sale
                            SET valid = 1
                            WHERE sale_id = '%s'""" % int(data)
                        )
        self.obj.commit()   
        
    def export(self, dateRange):
        """
            @dateRange = ['01-12-2009 00:00:00', '01-01-2010 00:00:00']
        """
        
        filters = ''
        dateFrom = dateRange[0]
        dateTo = dateRange[1]
        
        filters = " WHERE sale.date between '%s' and '%s' " % (dateFrom, dateTo)

        sql = """SELECT sale.sale_id as Numero,
                        ticket.description as Entrada, 
                        ticket.price as Precio, 
                        sale_ticket.cant as Cantidad,
                        ticket.price * sale_ticket.cant as Total,
                        CASE sale.valid 
                            WHEN  0 THEN 'NO'
                            ELSE 'SI'
                        END as Anulado,
                        sale.date as Fecha_Hora,
                        user.username as Usuario
                 FROM user join sale 
                    ON user.user_id = sale.user_id 
                 JOIN sale_ticket
                    ON sale.sale_id = sale_ticket.sale_id 
                 JOIN ticket 
                    ON sale_ticket.ticket_id = ticket.ticket_id"""
        
        filters += " ORDER by sale.date "
        query = sql + filters
        self.cur.execute(query)
        result = self.cur.fetchall()							

        c = []		
        for i in range(len(result)):
            dict = {}
            dict['id'] = result[i][0]
            dict['description'] = result[i][1]
            dict['price'] = result[i][2]
            dict['cant'] = result[i][3]
            dict['total'] = result[i][4]
            dict['anulado'] = result[i][5]
            dict['datetime'] = result[i][6]
            dict['username'] = result[i][7]
            c.append(dict)

        return c
