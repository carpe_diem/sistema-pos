# -*- coding: ISO-8859-1 -*-


class ObservableNotObject:
    """Observable Class.
    Subclasses MUST define a messages member which is a list of message names(strings).
    """

    def __init__(self):
        self.observersDict = dict()
        for msgName in self.messages:
            self.observersDict[msgName] = []

    def subscribeObserver(self, msgName, observer):
        "Returns an identifier which will be used on unsubscribe"
        self.observersDict[msgName].append(observer)
        return len(self.observersDict[msgName]) - 1

    def unsubscribeObserver(self, msgName, observerKey):
        self.observersDict[msgName][observerKey] = None

    def _notifyMessage(self, msgName, *msgData):
        observers = self.observersDict[msgName]
        [obs(*msgData) for obs in observers if obs]


class Observable(object, ObservableNotObject):

    def __init__(self):
        ObservableNotObject.__init__(self)


def subscribeUnsubscribe(observable, msgName, observer):
    key = observable.subscribeObserver(msgName, observer)
    return lambda: observable.unsubscribeObserver(msgName, key)


