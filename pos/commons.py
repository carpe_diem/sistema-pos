# -*- coding: ISO-8859-1 -*-
import re
import datetime
from dbDriver import Tickets
import ConfigParser


class Money():
    
    def __init__(self, value):
        self.value = value
    
    def toString(self):
        return '$ ' + str(self.value)

    def toFloat(self):
        regex = "\d+\.\d+"
        return re.findall(regex, self.value)


class saleDetails():

    def __init__(self):
        self.data = Tickets().findTicket()

    def getDescription(self):
        desc = []
        dict = {}
        z = 0
        for id in self.data:
            desc.append(id['description'])
            dict[z] = [id['id'], id['description']]
            z += 1
        return dict, desc
        
    def getUnitPrice(self, desc):
        return self.data[desc]['price']
        
    def getPrice(self, desc, cant):
        price = self.getUnitPrice(desc)
        return  '$ ' + str(price * cant)


class dateFunctions():
    
    def dateFormat(self, plain_date):
        d = self.convertPlain(plain_date)
        if self.IsValid(d):
            return datetime.date(year=int(d[4:]), month=int(d[2:4]), day=int(d[0:2]))
        else:
            return None

    def convertPlain(self, date):
        date_plain = date.replace("/","").replace("-","")
        #~ date_plain = date.replace("-","")
        return date_plain

    def IsValid(self, s):
        ret = False
        try:
            d = datetime.date(year=int(s[4:]), month=int(s[2:4]), day=int(s[0:2]))
            if d.year >= 1900:
                ret = True
        except:
            pass
        return ret

def convert_ticket_number(number):
    """ Return the ticket number formated """

    def complete_zeros(num):
        max_num = 8
        len_num = len(num)
        zeros = max_num - len_num
        return ('0' * zeros) + num

    cfgFile = '/usr/local/pos/config.cfg'
    config = ConfigParser.SafeConfigParser()
    resultLst = config.read(cfgFile)
    number = str(number)
    serie = config.get('Serie','suc')
    number = complete_zeros(number)
    ticket_number = serie + "-" + number
    return ticket_number

