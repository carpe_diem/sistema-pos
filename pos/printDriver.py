# -*- coding: ISO-8859-1 -*-
import parallel
import time
import  wx


PRINT_SUCCES = 0
PRINT_ERROR = 1
PRINT_OUT_PAPER = 2


class printerParallel:
    def __init__(self):
        self.setPort()
        
    def setPort(self):
        try:
            self.port = parallel.Parallel()

        except:
            dlg = wx.MessageDialog(None, 'Hay un problema con la conexion a la impresora', 'Error', wx.OK | 
                wx.ICON_ERROR)
            dlg.ShowModal()

    def status(self):
#       if self.port.getInError():
#            dlg = wx.MessageDialog(None, 'La impresora esta apagada o no conectada', 'Error', wx.OK | 
#                wx.ICON_ERROR)
#            result = dlg.ShowModal()
#            if result == wx.ID_OK:
#                return 0 
       if self.port.getInPaperOut():
            dlg = wx.MessageDialog(None, 'La impresora no tiene papel o esta apagada', 'Error', wx.OK | 
                wx.ICON_ERROR)
            result = dlg.ShowModal()
            if result == wx.ID_OK:
                return 0
       else: 
            return 1

    def sendPrinter(self, data):
        status = self.status()
        if status != 0:
            for c in data:
                self.port.setData(ord(c))
                time.sleep(0.001)
                self.port.setDataStrobe(1)
                self.port.setDataStrobe(0)
        return status



        
