#!/bin/bash

cp script/pos-start /usr/local/bin/
cp script/report-pos /usr/local/bin/

mkdir /usr/local/pos
cp config.cfg.example /usr/local/pos
mkdir /usr/local/pos/db
cp  db/clean.db /usr/local/pos/db/filo.db
cp -r pos /usr/lib/python2.5/site-packages/

chmod 777 /usr/local/pos/config.cfg
chmod 777 /usr/local/pos/db/
chmod 777 /usr/local/pos/db/filo.db
#chmod 777 /usr/local/pos/db/filo.clean
chmod 755 /usr/local/bin/pos-start
chmod 755 /usr/local/bin/report-pos
