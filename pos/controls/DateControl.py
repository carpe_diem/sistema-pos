# -*- coding: ISO-8859-1 -*-
from wx import *
#from wxPython.lib.maskededit import *
from wx.lib.masked.maskededit import *
from wx.lib.masked import *
import datetime
import calendar
from pos.utils.helpers import dateDelta
from pos.utils.patterns import ObservableNotObject, subscribeUnsubscribe


class DateCtrl(ObservableNotObject, TextCtrl):
    messages = ["textChanged"]

    def __init__(self, parent, id, initialValue=None):
        ObservableNotObject.__init__(self)
        TextCtrl.__init__(self, parent, id, autoformat="EUDATEDDMMYYYY/")
        self._AddNavKey("/", handler=self._OnChangeField)
        if wx.VERSION < (2, 6): # wx-2.6: no funciona, sale del panel
            self._SetKeycodeHandler(WXK_TAB, lambda evt: evt.Skip())
        self._SetKeycodeHandler(WXK_RETURN, lambda evt: evt.Skip())
        if initialValue:
            self.SetValue(initialValue.strftime("%d/%m/%Y"))
        EVT_KEY_DOWN(self, self._onChar)

    def _onChar(self, event):
        """Ctrl + H: completa el día de hoy
        Flecha arriba: suma un día
        Flecha abajo: resta un día
        Shift + Flecha arriba: suma un mes
        Shift + Flecha abajo: resta un mes
        """

        skip = True  # seguir procesando el evento al salir
        if event.GetKeyCode() == ord('H') and event.ControlDown():
            self.setValue(datetime.date.today())

        elif event.ShiftDown() and event.GetKeyCode() == WXK_UP:
            if self.getValue():
                self.setValue(dateDelta(self.getValue(), months=1))
        elif event.ShiftDown() and event.GetKeyCode() == WXK_DOWN:
            if self.getValue():
                self.setValue(dateDelta(self.getValue(), months=-1))
        elif event.GetKeyCode() == WXK_UP:
            if self.getValue():
                self.setValue(self.getValue() + datetime.timedelta(days=1))
        elif event.GetKeyCode() == WXK_DOWN:
            if self.getValue():
                self.setValue(self.getValue() - datetime.timedelta(days=1))

        elif event.GetKeyCode() == WXK_TAB and wx.VERSION >= (2, 6):
            # wx-2.6: previene "navegar" dentro del control
            # (pasar al siguiente control dentro del panel)
            flags = wx.NavigationKeyEvent.IsForward
            if event.ShiftDown():
                flags = wx.NavigationKeyEvent.IsBackward
            if event.ControlDown():
                flags |= wx.NavigationKeyEvent.WinChange
            self.Navigate(flags)
            skip = False

        if skip:
            event.Skip()

    def IsValid(self, *args):
        ret = False
        if TextCtrl.IsValid(self, *args):
            try:
                s = self.GetPlainValue()
                d = datetime.date(year=int(s[4:]), month=int(s[2:4]), day=int(s[0:2]))
                if d.year >= 1900:
                    ret = True
            except:
                pass
        return ret

    def getValue(self):
        "Returns the date"
        if self.IsEmpty() or not self.IsValid():
            return None
        else:
            s = self.GetPlainValue()
            return datetime.date(year=int(s[4:]), month=int(s[2:4]), day=int(s[0:2]))

    def setValue(self, value):
        if value and value.year >= 1900:
            self.SetValue(value.strftime("%d/%m/%Y"))
        else:
            self.ClearValue()

    def _daysOfMonth(self, value):
        firstweekday, days = calendar.monthrange(value.year, value.month)
        return days

    def _Change(self):
        self._notifyMessage("textChanged", self.getValue())
        return True


