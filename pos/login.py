import wx
from mainWindow import MainFrame
from dbDriver import Users


ID_OKAY = 1
ID_CANCEL = 2


class login(wx.Frame):
    def __init__(self, icondir):
        wx.Frame.__init__(self, None, -1, 'Login', size=(300, 150))
        self.icondir = icondir
        panel = wx.Panel(self)
        
        userLbl = wx.StaticText(panel, -1, "Usuario")
        self.user = wx.TextCtrl(panel, -1, "", size=(175, -1))
        self.user.SetFocus()
        
        passwdLbl = wx.StaticText(panel, -1, "Password:")
        self.passwd = wx.TextCtrl(panel, -1, "", size=(175, -1), style=wx.TE_PASSWORD)
        
        okayBtn = wx.Button(panel, ID_OKAY, "&Aceptar")
        okayBtn.SetDefault() 
        cancelBtn = wx.Button(panel, ID_CANCEL, "&Cancelar")
        
        mainSizer = wx.BoxSizer(wx.VERTICAL) 
        addrSizer = wx.FlexGridSizer(cols=2, hgap=6, vgap=6)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(userLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.user, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(passwdLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.passwd, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(okayBtn)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        
        mainSizer.Add(btnSizer, 0, wx.EXPAND|wx.ALL, 10)
        panel.SetSizer(mainSizer)

        self.Bind(wx.EVT_BUTTON, self.OnCancelWindow, id=ID_CANCEL)
        self.Bind(wx.EVT_BUTTON, self.OnAceptWindow, id=ID_OKAY)
        
        self.Centre()

         
    def OnCancelWindow(self, evt):
        self.Close()

    def OnAceptWindow(self, evt):
        data = {'passwd': self.passwd.GetValue(), 'username': self.user.GetValue()}
        validate = Users().login(data)
        if validate is None:
            dlg = wx.MessageDialog(None, 'El nombre de usuario o Contrasena es incorrecto', 'Login Incorrecto', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            self.passwd.SetValue("")

        else:
            options = {'username': validate[0], 'admin':validate[2], 'icondir': self.icondir}
            #~ load = MainFrame("Prueba - Programa FILO", options, (50, 50), (800, 600))
            load = MainFrame(options, (50, 50), (1024, 768))
            load.Show()
            self.Close()
