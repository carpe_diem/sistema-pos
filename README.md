Sistema POS version 1.4
------------------------

Creado en 2008

About
------------------------
Sistema POS es un sistema para realizar las ventas de entradas e imprimirla.
Esta escrito en python/wxpython.

Este sistema fue escrito en 2008 a pedido de la Universidad de Buenos Aires, 
Facultad de Filosofía y Letras, para la venta de tickets de el museo de tilcara y 
Púcara de tilcara.



Installation requirements
------------------------------
Para correr el programa se necesitan:
* python >= version 2.5       (Conseguir desde http://www.python.org)
* wxpython >= version 2.8     (conseguir desde http://www.wxpython.org)
* pyparallel                  (conseguir desde http://pyserial.sourceforge.net/pyparallel.html python-parallel en debian)
    
Python 2.4 no incluye los modulos de sqlite3, entonces se necesita
el modulo pysqlite.
* pysqlite >= version 2.3.2   (Conseguir desde http://www.pysqlite.org)

Modulo pyParalle en Gnu/Linux:
* El Modulo lp debe ser bajado (rmmod lp)
* El modulo ppdev debe ser cargado (modprobe ppdev).
* Se deben tener permisos para escribir en /dev/parport0. La mayoria de las Distribuciones tienen un
    grupo lp. Simplemente agregar el usuario a este grupo.

Installing POS
------------------------------
El sistema POS puede ejecutarse sin una instalacion ($ python pos-start.py).
Pero provee un script de instalacion setup.py. solo correr como root:
$ python setup.py install
