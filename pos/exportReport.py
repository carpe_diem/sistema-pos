# -*- coding: ISO-8859-1 -*-
from dbDriver import Users, Sale 
import sys
import os
import random
import csv
import ConfigParser
from getpass import getpass
import wx
import datetime

ID_OKAY = 1
ID_CANCEL = 2
ID_EXP = 101
ID_CLOSE = 2
ID_CBO_MONTH = 115

cfgFile = '/usr/local/pos/config.cfg'
config = ConfigParser.SafeConfigParser()
resultLst = config.read(cfgFile)
export_dir = config.get('Settings','export_dir')

def getDate(month, year):
    aux , aux2 = '', ''
    month2 = month
    year2 = year

    if len(str(month)) == 1:
        aux = 0
        if month != 9:
            aux2 = 0
    if month == 12:
        month2 = 1
        year2 = year + 1
        aux2 = 0
    else:
        month2 = month + 1

    date_from = "%s-%s%s-01 00:00:00" % (year,aux, month)
    date_to = "%s-%s%s-01 00:00:00" % (year2, aux2, month2)
    return date_from, date_to 

def get_rand_name(baseName):
    randName = ""
    while 1:
        filename = "%s/%s%s.csv" % (export_dir, randName, baseName)
        short_filename = "%s%s.csv" % (randName, baseName)
        if os.access(filename, os.F_OK):
            # Already exists a file with that name
            randName = "%d-" % random.randint(1, 9999)
        else:
            break
    return filename, short_filename
    
def writeFile(date):
    
    data = Sale().export(date)
    
    file_name, short_filename = get_rand_name(datetime.date.today())    
    dest = csv.writer(open(file_name, 'w'), delimiter=';', quotechar=';')    
    row = []
    dest.writerow(['Nro.Ticket'] + ['Descripcion'] + ['Precio'] + ['Cantidad'] + ['Total'] + ['Anulada'] + ['Fecha-Hora'] + ['Usuario'])
    for i in data:
        dest.writerow(
                [str(i['id'])] + 
                [str(i['description'])] +
                [str(i['price']).replace('.',',')] +
                [str(i['cant'])] +
                [str(i['total']).replace('.',',')] +
                [str(i['anulado'])] +
                [str(i['datetime'])] +
                [str(i['username'])]
            )
    return short_filename


class exportLogin(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, 'Login', size=(300, 150))
        panel = wx.Panel(self)
        
        userLbl = wx.StaticText(panel, -1, "Usuario")
        self.user = wx.TextCtrl(panel, -1, "", size=(175, -1))
        self.user.SetFocus()
        
        passwdLbl = wx.StaticText(panel, -1, "Password:")
        self.passwd = wx.TextCtrl(panel, -1, "", size=(175, -1), style=wx.TE_PASSWORD)
        
        okayBtn = wx.Button(panel, ID_OKAY, "&Aceptar")
        okayBtn.SetDefault() 
        cancelBtn = wx.Button(panel, ID_CANCEL, "&Cancelar")
        
        mainSizer = wx.BoxSizer(wx.VERTICAL) 
        addrSizer = wx.FlexGridSizer(cols=2, hgap=6, vgap=6)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(userLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.user, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(passwdLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.passwd, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(okayBtn)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        
        mainSizer.Add(btnSizer, 0, wx.EXPAND|wx.ALL, 10)
        panel.SetSizer(mainSizer)

        self.Bind(wx.EVT_BUTTON, self.OnCancelWindow, id=ID_CANCEL)
        self.Bind(wx.EVT_BUTTON, self.OnAceptWindow, id=ID_OKAY)
        
        self.Centre()

         
    def OnCancelWindow(self, evt):
        self.Close()

    def OnAceptWindow(self, evt):
        data = {'passwd': self.passwd.GetValue(), 'username': self.user.GetValue()}
        validate = Users().login(data)
        if validate is None:
            dlg = wx.MessageDialog(None, 'El nombre de usuario o Contrasena es incorrecto', 'Login Incorrecto', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            self.passwd.SetValue("")
        elif validate[2] != 1:
            dlg = wx.MessageDialog(None, 'El usuario no tiene permisos para ejecutar el Resumen de ventas', 'Login Incorrecto', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            self.passwd.SetValue("")
        else:
            load = export("Periodo")
            load.Show()
            self.Close()


class export(wx.Frame):
    
    def __init__(self, title):
        wx.Frame.__init__(self, None, -1, title, size=(250, 150))

        months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 
                  'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']

        panel = wx.Panel(self)
        
        yearLbl = wx.StaticText(panel, -1, "Año: ")
        self.year = wx.SpinCtrl(panel, -1, '', size=(100, -1), min=2009, max=2050)
        
        monthLbl = wx.StaticText(panel, -1, "Mes: ")
        self.month = wx.ComboBox(panel, ID_CBO_MONTH, value=months[0], choices=months, size=(150, -1), style=wx.CB_DROPDOWN|wx.CB_READONLY)
        self.month.SetFocus()
        
        expBtn = wx.Button(panel, ID_EXP, "Exportar")
        cancelBtn = wx.Button(panel, ID_CLOSE, "Cancelar")


        mainSizer = wx.BoxSizer(wx.VERTICAL) 
        addrSizer = wx.FlexGridSizer(cols=2, hgap=6, vgap=6)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(yearLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.month, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(monthLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.year, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(expBtn)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        
        mainSizer.Add(btnSizer, 0, wx.EXPAND|wx.ALL, 10)
        panel.SetSizer(mainSizer)

        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.onExport, id=ID_EXP)
        
        self.Centre()
        
    def OnCloseWindow(self, evt):
        self.Close()
    
    def onExport(self, evt):
        #~ print self.month.GetCurrentSelection() +1
        if self.month.GetCurrentSelection() == -1:
            month = self.month.GetCurrentSelection() + 2
        else:
            month = self.month.GetCurrentSelection() + 1
            
        year = self.year.GetValue()

        date_from, date_to = getDate(month, year)
        date = [date_from, date_to]
        res = writeFile(date)

        dlg = wx.MessageDialog(None, """Se genero correctamente el archivo %s 
                para el periodo %s del año %s """ % (res, month, year), 
            'Resumen de Ventas', wx.OK | wx.ICON_EXCLAMATION)
        dlg.ShowModal()
        self.Close()


if __name__ == '__main__':
    app = wx.PySimpleApp()
    frame = exportLogin()
    frame.Show()
    app.MainLoop()



