#!/bin/bash
# Correr como root
# Copiar todo a un directorio temporal en la forma pos-version
# Ver si lo podemos sacar del tag

BUILD_DIR=$(mktemp -d /tmp/pos-build.XXXXXXXXXX)

echo "Creando paquete para pos en el directorio /tmp"
echo "Creando "$BUILD_DIR
echo "Checkouteando versión xx en  "$BUILD_DIR
bzr co --lightweight . $BUILD_DIR
echo "Cambiando a "$BUILD_DIR
pushd $BUILD_DIR
echo "Creando un config.cfg"
cp config.cfg.example config.cfg
dpkg-buildpackage
echo "Borrando rastros ..."
rm -r $BUILD_DIR
echo "Regresando al directorio actual"
popd
echo "Finalizado"
