# -*- coding: ISO-8859-1 -*-
import ConfigParser
import wx

ID_ABOUT = 1

cfgFile = '/usr/local/pos/config.cfg'
config = ConfigParser.SafeConfigParser()
resultLst = config.read(cfgFile)

class AboutDialogBox(wx.AboutDialogInfo):
    def __init__(self, parent, id, title):
        wx.AboutBox.__init__(self, parent, id, title, size=(260, 200))

        description = "Sistema de venta de entradas"

        info = wx.AboutDialogInfo()

        info.SetIcon(wx.Icon('pos/icons/logo.png', wx.BITMAP_TYPE_PNG))
        info.SetName('POS')
        info.SetVersion(config.get('Version','version'))
        info.SetDescription(description)
        info.SetCopyright('(C) 2009 ')
        info.SetWebSite('http://')
        #~ info.SetLicence(licence)
        info.AddDeveloper('Alberto Paparelli')
        #~ info.AddDocWriter('Alberto Paparelli')
        #~ info.AddArtist('Alberto Paparelli')
        #~ info.AddTranslator('Alberto Paparelli')
        wx.AboutBox(info)
