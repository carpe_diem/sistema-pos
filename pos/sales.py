# -*- coding: ISO-8859-1 -*-
import  wx
from dbDriver import Tickets, Sale
from commons import Money, saleDetails, dateFunctions,\
    convert_ticket_number
import sys
from wx.lib.mixins.listctrl import ColumnSorterMixin
from commons import Money
import ConfigParser
import datetime
from  printDriver import printerParallel
from controls.DateControl import DateCtrl


ID_CLOSE = 102
ID_ADD = 103
ID_DEL = 104
ID_PRINT = 105
ID_CBO_PROD = 140
ID_SAVE = 150
ID_SEARCH = 108


class SortedListCtrl(wx.ListCtrl, ColumnSorterMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT)
        ColumnSorterMixin.__init__(self, len({}))
        self.itemDataMap = {}

    def GetListCtrl(self):
        return self
    

class NewSaleWindow(wx.MDIChildFrame):
    def __init__(self, parent, title, options, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)
        self.options = options
        
        panel = wx.Panel(self)
        # First create the controls
        topLbl = wx.StaticText(panel, -1, "Informacion de venta")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        
        self.mapeo, sales_products= saleDetails().getDescription()
        
        productLbl = wx.StaticText(panel, -1, "Producto:")
        self.product = wx.ComboBox(panel, ID_CBO_PROD, value = 'Elija una Entrada', choices=sales_products, size=(300, -1), style= wx.CB_DROPDOWN|wx.CB_READONLY)
        self.product.SetFocus()
        
        cantLbl = wx.StaticText(panel, -1, "Cantidad:")
        self.cantidad = wx.SpinCtrl(panel, -1, '', size=(50, -1), min=1, max=100)

        priceLbl = wx.StaticText(panel, -1, "Precio:")
        self.price = wx.TextCtrl(panel, -1, "$ 0,00", size=(80,-1))
        self.price.Enable(False)
        
        addBtn = wx.Button(panel, ID_ADD, "Agregar")
        delBtn = wx.Button(panel, ID_DEL, "Quitar")
        
        self.list = SortedListCtrl(panel)
        self.list.InsertColumn(0, 'Codigo', width=60)
        self.list.InsertColumn(1, 'Descripcion', width=300)
        self.list.InsertColumn(2, 'Cantidad', wx.LIST_FORMAT_CENTER, width=130)
        self.list.InsertColumn(3, 'Precio unitaro',wx.LIST_FORMAT_CENTER, width=130)
        self.list.InsertColumn(4, 'Precio total', wx.LIST_FORMAT_CENTER, 130)

        self.totLbl = wx.StaticText(panel, -1, "Total:")
        self.tot = wx.TextCtrl(panel, -1, "$ 0,00", size=(80,-1))
        self.tot.Enable(False)
        
        saveBtn = wx.Button(panel, ID_SAVE, "Guardar")
        cancelBtn = wx.Button(panel, ID_CLOSE, "Cancelar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addSizer.AddGrowableCol(1)
        
        addSizer.Add(productLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.product, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        addSizer.Add(cantLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.cantidad, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        addSizer.Add(priceLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.price, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(addBtn)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(delBtn)
        mainSizer.Add(btnSizer, 0, wx.RIGHT|wx.BOTTOM, 10)
        
        listSizer = wx.BoxSizer(wx.HORIZONTAL)
        listSizer.Add(self.list, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
        mainSizer.Add(listSizer, 1, wx.EXPAND|wx.ALL, 10)

        totSizer = wx.BoxSizer(wx.HORIZONTAL)
        totSizer.Add(self.totLbl, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        totSizer.Add((20,20), 1)
        totSizer.Add(self.tot, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        mainSizer.Add(totSizer, 0, wx.ALIGN_RIGHT|wx.BOTTOM, 10)
        totSizer.Add((20,20), 1)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        btnSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer2.Add((20,20), 1)
        btnSizer2.Add(saveBtn)
        btnSizer2.Add((20,20), 1)
        btnSizer2.Add(cancelBtn)
        btnSizer2.Add((20,20), 1)

        mainSizer.Add(btnSizer2, 0, wx.ALIGN_RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)
        
        self.Bind(wx.EVT_BUTTON, self.OnAddProduct, id=ID_ADD)
        self.Bind(wx.EVT_BUTTON, self.OnDelProduct, id=ID_DEL)
        self.Bind(wx.EVT_COMBOBOX, self.onProductChange)
        self.Bind(wx.EVT_SPINCTRL, self.onProductChange)
        self.Bind(wx.EVT_BUTTON, self.OnSaveWindow, id=ID_SAVE)
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        
        self.Show(True)


    def OnCloseWindow(self, evt):
        self.Close()       
         
    def print_blank(self, lines):
        return '\n' * lines

    def print_separator(self):
        return '-' * 40

    def centerText(self, text):
        if len(text) % 2 == 0:            
            spaces = ' ' * ((40 - len(text)) / 2)
            spaces2 = spaces
        else:
	    spaces = ' ' * ((40 - len(text)) / 2)
            spaces2 = ' ' * ((40 - len(text)) / 2) + ' '
	return spaces + text + spaces2


    def printTicket(self, dict):
        cfgFile = '/usr/local/pos/config.cfg'
        config = ConfigParser.SafeConfigParser()
        resultLst = config.read(cfgFile)
        try:
            header1 = config.get('Print','header1')
            header1 = self.centerText(header1)
        except:
            header1 = None
        try:
            header2 = config.get('Print','header2')
            header2 = self.centerText(header2)
        except:
            header2 = None
        try:
            header3 = config.get('Print','header3')
            header3 = self.centerText(header3)
        except:
            header3 = None
        try:
            footer1 = config.get('Print','footer1')
            footer1 = self.centerText(footer1)
        except:
            footer1 = None
        try:
            footer2 = config.get('Print','footer2')
            footer2 = self.centerText(footer2)
        except:
            footer2 = None
        try:
            footer3 = config.get('Print','footer3')
	    footer3 = self.centerText(footer3)
        except:
            footer3 = None
        to_print = ''
        p = printerParallel()        
    
        to_print += self.print_blank(1)
        to_print += self.print_separator()
        if header1:
            to_print += self.print_blank(1)
            to_print += header1
            to_print += self.print_blank(1)
            to_print += self.print_separator()
            to_print += self.print_blank(1)
        if header2:
            to_print += header2
            to_print += self.print_blank(1)
        if header3:
            to_print += header3
            to_print += self.print_blank(1)
        if header2 or header3:
            to_print += self.print_separator()
            to_print += self.print_blank(1)

        ticket_number = Sale().get_next_id()
        ticket_number = convert_ticket_number(ticket_number)

        to_print += (' ' * 27) + ticket_number
        to_print += self.print_blank(2)

        to_print += 'Cant. descripcion\t\tPrecio'
        to_print += self.print_blank(2)
        a = 0
        for i in range(len(dict)):
            cant = dict[i][0]
            desc = dict[i][1]
            price = dict[i][2]
            spaces = ' ' * ((5 - len(cant)) / 2)
            if len(cant) % 2 == 0:
                spaces = ' ' * ((5 - len(cant)) / 2)
                spaces2 = spaces
            else:
                spaces = ' ' * ((5 - len(cant)) / 2)
                spaces2 = ' ' * ((5 - len(cant)) / 2) + ' '
            cant = spaces + cant + spaces2

            spaces = ' ' * ((25 - len(desc)))
            desc = desc + spaces

            spaces = ' ' * ((8 - len(price)))
            price = spaces + price

            to_print += "%s%s%s" % (cant, desc, price)
            to_print += self.print_blank(1)
            a += float(Money(dict[i][2]).toFloat()[0])

        to_print += self.print_blank(1)
        to_print += "                          -------------\n"
        to_print += "                 TOTAL        %s" % Money(a).toString()
        to_print += self.print_blank(1)
        to_print += self.print_separator()
        to_print += self.print_blank(1)
        if footer1:
            to_print += footer1
            to_print += self.print_blank(1)
            to_print += self.print_separator()
            to_print += self.print_blank(1)
        if footer2:
            to_print += footer2
            to_print += self.print_blank(1)
        if footer3:
            to_print += footer3
            to_print += self.print_blank(1)
        if footer2 or footer3:
            to_print += self.print_separator()
            to_print += self.print_blank(1)

        to_print += "%s\t\t\t%s" % (datetime.date.today(), datetime.datetime.now().strftime("%H:%M:%S"))
        to_print += self.print_blank(10)
        self.retPrint = p.sendPrinter(to_print)

    def OnSaveWindow(self, evt):
        dict = {}
        printPreview = {}
        rows = self.list.GetItemCount()
        if rows == 0:
            dlg = wx.MessageDialog(None, 'Debe agregar items a la venta', 'Ventas', wx.OK | 
                wx.ICON_EXCLAMATION)
            dlg.ShowModal()
        else:
            for i in range(rows):
                row = []
                rowPrint = []  
                # Id del ticket y cantidad
                row.append(self.list.GetItem(i,0).GetText())
                row.append(self.list.GetItem(i,2).GetText())
                dict[i] = row
                rowPrint.append(self.list.GetItem(i,2).GetText())
                rowPrint.append(self.list.GetItem(i,1).GetText())
                rowPrint.append(self.list.GetItem(i,4).GetText())
                printPreview[i] = rowPrint              
            
            self.printTicket(printPreview)                
            if self.retPrint == 1:
                Sale().newSale(dict, self.options)
                self.OnCloseWindow(evt)

    def OnAddProduct(self, evt):
        entradas = {}
        
        if self.product.GetCurrentSelection() == -1:
            dlg = wx.MessageDialog(None, 'Debe seleccionar un item', 'Ventas', wx.OK | 
                wx.ICON_EXCLAMATION)
            dlg.ShowModal()
        else:
            unit_price = float(Money(self.price.GetValue()).toFloat()[0]) / self.cantidad.GetValue()
            rows = self.list.GetItemCount()
            productId = self.mapeo[self.product.GetCurrentSelection()][0]
            
            if rows:
                for i in range(rows):
                    if str(productId) == self.list.GetItem(i,0).GetText():
                        ent = i
                        self.list.DeleteItem(i)
                    else:
                        ent = len(entradas) + 1
            else:       
                ent = len(entradas) + 1
            
            entradas[ent] = (str(productId), self.product.GetValue(),
                str(self.cantidad.GetValue()), Money(unit_price).toString(), self.price.GetValue())

            self.items = entradas.items()

            for key, data in self.items:
                index = self.list.InsertStringItem(sys.maxint, data[0])
                self.list.SetStringItem(index, 1, data[1])
                self.list.SetStringItem(index, 2, data[2])
                self.list.SetStringItem(index, 3, data[3])
                self.list.SetStringItem(index, 4, data[4])
                self.list.SetItemData(index, key)
            
            self.clearData()
            self.totalClac()
         
    def totalClac(self):
        total_price = 0
        for i in range(self.list.GetItemCount()):
            total_price += float(Money(self.list.GetItem(i,4).GetText()).toFloat()[0])
        
        self.tot.SetValue(Money(total_price).toString())
            
    def clearData(self):
        self.product.SetValue('Elija una Entrada')
        self.price.SetValue('$ 0,00')
        self.cantidad.SetValue(1)
        
    def OnDelProduct(self, evt):
        index = self.list.GetFocusedItem()
        self.list.DeleteItem(index)
        self.totalClac()
        
    def onProductChange(self, evt):
        self.price.SetValue(saleDetails().getPrice(self.product.GetCurrentSelection(), self.cantidad.GetValue()))


class SaleDetailWindow(wx.MDIChildFrame):
    def __init__(self, parent, title, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)
        panel = wx.Panel(self)


        topLbl = wx.StaticText(panel, -1, "Resumen de ventas")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))

        dateFromLbl = wx.StaticText(panel, -1, "Desde: ")
        #self.dateFrom = wx.TextCtrl(panel, -1, "", size=(100,-1));
        self.dateFrom = DateCtrl(panel, -1);

        self.dateFrom.SetFocus()

        dateToLbl = wx.StaticText(panel, -1, "Hasta: ")
        #self.dateTo = wx.TextCtrl(panel, -1, "", size=(100,-1));
        self.dateTo = DateCtrl(panel, -1);

        searchBtn = wx.Button(panel, ID_SEARCH, "Buscar")
        delBtn = wx.Button(panel, ID_DEL, "Eliminar")

        self.list = SortedListCtrl(panel)
        self.list.InsertColumn(0, 'Numero', width=100)
        self.list.InsertColumn(1, 'Entrada', width=400)
        self.list.InsertColumn(2, 'Precio', wx.LIST_FORMAT_CENTER, width=150)
        self.list.InsertColumn(3, 'Cantidad',wx.LIST_FORMAT_CENTER, width=100)
        self.list.InsertColumn(4, 'Total', wx.LIST_FORMAT_CENTER, 150)
        self.list.InsertColumn(5, 'Anulado',wx.LIST_FORMAT_CENTER, width=70)
        self.list.InsertColumn(6, 'Fecha_Hora', wx.LIST_FORMAT_CENTER, 200)
        self.list.InsertColumn(7, 'Usuario', wx.LIST_FORMAT_CENTER, 200)

        closeBtn = wx.Button(panel, ID_CLOSE, "Cerrar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addSizer.AddGrowableCol(1)
        
        addSizer = wx.BoxSizer(wx.HORIZONTAL)
        addSizer.Add(dateFromLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.dateFrom, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add((20,20), 1)
        addSizer.Add(dateToLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.dateTo, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add((20,20), 1)
        addSizer.Add(searchBtn)
        addSizer.Add((20,20), 1)

        mainSizer.Add(addSizer, 0, wx.RIGHT|wx.BOTTOM, 10)
        
        listSizer = wx.BoxSizer(wx.HORIZONTAL)
        listSizer.Add(self.list, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
        mainSizer.Add(listSizer, 1, wx.EXPAND|wx.ALL, 10)

        btnSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer2.Add(delBtn)
        btnSizer2.Add((60,20), 1)
        btnSizer2.Add(closeBtn)
        btnSizer2.Add((20,20), 1)

        mainSizer.Add(btnSizer2, 0, wx.ALIGN_RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)
        
        self.Bind(wx.EVT_BUTTON, self.OnSearchProduct, id=ID_SEARCH)
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnSaleDelete, id=ID_DEL)
        
        self.Show(True)


    def OnCloseWindow(self, evt):
        self.Close()

    def OnSearchProduct(self, evt):
        self.cleanListCtrl()
        index = self.list.GetFocusedItem()
        self.list.DeleteItem(index)

        date_from = dateFunctions().dateFormat(self.dateFrom.GetValue())
        date_to = dateFunctions().dateFormat(self.dateTo.GetValue())

        #~ if date_from is None and date_to is None:
            #~ dlg = wx.MessageDialog(self, 'Se va a realizar la busqueda sin filtros, esta seguro?', 'Resumen de ventas', wx.YES_NO | 
            #~ wx.NO_DEFAULT | wx.ICON_QUESTION)
            #~ if dlg.ShowModal() == wx.ID_YES:
                #~ pass
                
        dateRange = [date_from, date_to]
        entradas = Sale().selectAll(dateRange)

        #~ if not entradas:
            #~ self.statusbar.SetStatusText('No se encontraron Resultados', 0)
        j = 0
        cont = 0
        for i in entradas:
            index = self.list.InsertStringItem(sys.maxint, str(i['id']))
            self.list.SetStringItem(index, 1, str(i['description']))
            self.list.SetStringItem(index, 2, str(i['price']))
            self.list.SetStringItem(index, 3, str(i['cant']))
            self.list.SetStringItem(index, 4, str(i['total']))
            self.list.SetStringItem(index, 5, str(i['anulado']))
            self.list.SetStringItem(index, 6, str(i['datetime']))
            self.list.SetStringItem(index, 7, str(i['username']))

            if j % 2 == 0:
                #~ self.list.SetItemBackgroundColour(index, "light grey")
                self.list.SetItemBackgroundColour(index, "#f9e7d8")
            else:
                self.list.SetItemBackgroundColour(index, "#fbf6d7")
            #~ if str(i['anulado']) == 'SI':
                #~ self.list.SetTextColour(index, "orange")
            if cont + 1 != len(entradas):
                if i['id'] == entradas[cont+1]['id']:
                    j += 2
                else:
                    j += 1
            else:
                    j += 1
            cont += 1

    def cleanListCtrl(self):
        for i in range(self.list.GetItemCount()):
           self.list.DeleteItem(0)
    
    def OnSaleDelete(self, evt):
        index = self.list.GetFocusedItem()
        num = self.list.GetItem(index,0).GetText()

        dlg = wx.MessageDialog(self, "Esta seguro que desea eliminar el Ticket Numero '%s' " % num, 'Eliminar Ticket', wx.YES_NO | 
        wx.NO_DEFAULT | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            #~ rows = self.list.GetItemCount()
            #~ del_list = []
            #~ print range(1, rows)
            #~ for i in range(rows):
                #~ if self.list.GetItem(i,0).GetText() == num:
                    #~ del_list.append(i)
            #~ del_list.sort()
            #~ del_list.reverse()
            #~ for i in del_list:
                #~ self.list.DeleteItem(i)

            Sale().cancelSale(num)
            self.OnSearchProduct(evt)

class SaleDetailUserWindow(wx.MDIChildFrame):
    def __init__(self, parent, user, title, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)
        panel = wx.Panel(self)
        self.user = user

        topLbl = wx.StaticText(panel, -1, "Resumen de ventas diario por usuario")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))

        dateLbl = wx.StaticText(panel, -1, "Fecha: ")
        #self.date = wx.TextCtrl(panel, -1, "", size=(100,-1));
        self.date = DateCtrl(panel, -1);
        self.date.SetFocus()

        searchBtn = wx.Button(panel, ID_SEARCH, "Buscar")
        self.printBtn = wx.Button(panel, ID_PRINT, "Imprimir")
        self.printBtn.Enable(False)

        self.list = SortedListCtrl(panel)
        self.list.InsertColumn(0, 'Entrada', width=400)
        self.list.InsertColumn(1, 'Cantidad',wx.LIST_FORMAT_CENTER, width=100)
        self.list.InsertColumn(2, 'Precio', wx.LIST_FORMAT_CENTER, width=150)
        self.list.InsertColumn(3, 'Total', wx.LIST_FORMAT_CENTER, 150)
        self.list.InsertColumn(4, 'Usuario', wx.LIST_FORMAT_CENTER, 200)
        self.list.InsertColumn(5, 'Fecha', wx.LIST_FORMAT_CENTER, 200)

        closeBtn = wx.Button(panel, ID_CLOSE, "Cerrar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addSizer.AddGrowableCol(1)

        addSizer = wx.BoxSizer(wx.HORIZONTAL)
        addSizer.Add(dateLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add(self.date, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
        addSizer.Add((20,20), 1)
        addSizer.Add(searchBtn)
        addSizer.Add((20,20), 1)

        mainSizer.Add(addSizer, 0, wx.RIGHT|wx.BOTTOM, 10)

        listSizer = wx.BoxSizer(wx.HORIZONTAL)
        listSizer.Add(self.list, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
        mainSizer.Add(listSizer, 1, wx.EXPAND|wx.ALL, 10)

        btnSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer2.Add(self.printBtn)
        btnSizer2.Add((60,20), 1)
        btnSizer2.Add(closeBtn)
        btnSizer2.Add((20,20), 1)

        mainSizer.Add(btnSizer2, 0, wx.ALIGN_RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)

        self.Bind(wx.EVT_BUTTON, self.OnSearchProduct, id=ID_SEARCH)
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnPrintWindow, id=ID_PRINT)

        self.Show(True)


    def OnCloseWindow(self, evt):
        self.Close()

    def OnSearchProduct(self, evt):
        self.cleanListCtrl()
        index = self.list.GetFocusedItem()
        self.list.DeleteItem(index)

        date = dateFunctions().dateFormat(self.date.GetValue())
        if not date:
            dlg = wx.MessageDialog(None, 'Debe poner una fecha valida',
                'Resumen de ventas', wx.OK | wx.ICON_EXCLAMATION)
            dlg.ShowModal()
        else:
            self.entradas = Sale().billing_user(date, self.user)

            j = 0
            cont = 0
            for i in self.entradas:
                index = self.list.InsertStringItem(sys.maxint, str(i['description']))
                self.list.SetStringItem(index, 1, str(i['cant']))
                self.list.SetStringItem(index, 2, str(i['price']))
                self.list.SetStringItem(index, 3, str(i['total']))
                self.list.SetStringItem(index, 4, str(i['user']))
                self.list.SetStringItem(index, 5, str(i['date']))

                if j % 2 == 0:
                    self.list.SetItemBackgroundColour(index, "#f9e7d8")
                else:
                    self.list.SetItemBackgroundColour(index, "#fbf6d7")
                cont += 1
            if self.entradas:
                self.printBtn.Enable(True)
            else:
                self.printBtn.Enable(False)

    def cleanListCtrl(self):
        for i in range(self.list.GetItemCount()):
           self.list.DeleteItem(0)

    def print_blank(self, lines):
        return '\n' * lines

    def print_separator(self):
        return '-' * 40

    def OnPrintWindow(self, evt):
        to_print = ''
        p = printerParallel()
        entradas = self.entradas

        to_print += self.print_blank(1)
        to_print += self.print_separator()
        to_print += self.print_blank(1)
        to_print += "Fecha Consultada: %s" %  entradas[0]['date']
        to_print += self.print_blank(1)
        to_print += "usuario: %s" % entradas[0]['user']
        to_print += self.print_blank(2)
        to_print += self.print_separator()
        to_print += self.print_blank(1)
        to_print += 'Cant. descripcion\t\tPrecio'
        to_print += self.print_blank(2)
        a = 0

        for i in range(len(entradas)):
            desc = entradas[i]['description']
            cant = str(entradas[i]['cant'])
            price = str(entradas[i]['price'])
            total = str(entradas[i]['total'])
            user = entradas[i]['user']
            date = entradas[i]['date']

            spaces = ' ' * ((5 - len(cant)) / 2)
            if len(cant) % 2 == 0:
                spaces = ' ' * ((5 - len(cant)) / 2)
                spaces2 = spaces
            else:
                spaces = ' ' * ((5 - len(cant)) / 2)
                spaces2 = ' ' * ((5 - len(cant)) / 2) + ' '
            cant = spaces + cant + spaces2

            spaces = ' ' * ((25 - len(desc)))
            desc = desc + spaces

            spaces = ' ' * ((8 - len(price)))
            price = spaces + price

            to_print += "%s%s%s" % (cant, desc, total)
            to_print += self.print_blank(1)
            a += float(Money(total).toFloat()[0])

        to_print += self.print_blank(1)
        to_print += "                          -------------\n"
        to_print += "                 TOTAL        %s" % Money(a).toString()
        to_print += self.print_blank(1)
        to_print += self.print_separator()
        to_print += self.print_blank(1)
        to_print += "%s\t\t\t%s" % (datetime.date.today(), datetime.datetime.now().strftime("%H:%M:%S"))
        to_print += self.print_blank(10)
        self.retPrint = p.sendPrinter(to_print)


