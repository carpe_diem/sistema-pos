# -*- coding: ISO-8859-1 -*-
import  wx
import decimal
from dbDriver import Tickets
from commons import Money, saleDetails


ID_CLOSE = 102
ID_NEW_PROD = 104
ID_EDIT_PROD = 104
ID_CBO_PROD = 140

class AddProductWindow(wx.MDIChildFrame):
    def __init__(self, parent, title, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)

        panel = wx.Panel(self)

        topLbl = wx.StaticText(panel, -1, "Agregar nuevo Entrada")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        
        productLbl = wx.StaticText(panel, -1, "Descripcion:")
        self.product = wx.TextCtrl(panel, -1, "", size=(250,-1));
        self.product.SetFocus()
        
        priceLbl = wx.StaticText(panel, -1, "Precio:")
        self.price = wx.TextCtrl(panel, -1, "", size=(100,-1));
        
        saveBtn = wx.Button(panel, ID_NEW_PROD, "Guardar")
        cancelBtn = wx.Button(panel, ID_CLOSE, "Cancelar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addrSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(productLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.product, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL, 100)

        addrSizer.Add(priceLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 50)
        addrSizer.Add(self.price, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(saveBtn)

        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        mainSizer.Add(btnSizer, 0, wx.RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)
        
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnNewProduct, id=ID_NEW_PROD)
        
    def OnCloseWindow(self, evt):
        self.Close()
        
    def OnNewProduct(self, evt):

        if self.product.GetValue().strip():
            try:
                price = self.price.GetValue()
                price2 = decimal.Decimal(price, 2)
                
                data = [self.product.GetValue().strip(), price2]
                Tickets().insertTicket(data)
                self.OnCloseWindow(evt)
            except:
                dlg = wx.MessageDialog(None, 'El precio no es correcto', 'Error', wx.OK | wx.ICON_ERROR)
                dlg.ShowModal()
        else:
            dlg = wx.MessageDialog(None, 'La descripcion no puede estar en blanco', 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()   
                 



class EditProductWindow(wx.MDIChildFrame):
    def __init__(self, parent, title, size, pos):
        wx.MDIChildFrame.__init__(self, parent, -1, title, size = size, pos =pos)

        self.mapeo, sales_products= saleDetails().getDescription()

        panel = wx.Panel(self)
        topLbl = wx.StaticText(panel, -1, "Cambiar precio")
        topLbl.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
        
        productLbl = wx.StaticText(panel, -1, "Producto:")
        self.product = wx.ComboBox(panel, ID_CBO_PROD, value = 'Elegir item a editar', choices=sales_products, size=(300, -1), style=wx.CB_DROPDOWN|wx.CB_READONLY)
        self.product.SetFocus()
        
        priceLbl = wx.StaticText(panel, -1, "Precio:")
        self.price = wx.TextCtrl(panel, -1, "", size=(100,-1));
        
        saveBtn = wx.Button(panel, ID_EDIT_PROD, "Guardar")
        cancelBtn = wx.Button(panel, ID_CLOSE, "Cancelar")

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(topLbl, 0, wx.ALL, 5)
        mainSizer.Add(wx.StaticLine(panel), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

        addrSizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        addrSizer.AddGrowableCol(1)
        
        addrSizer.Add(productLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        addrSizer.Add(self.product, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL, 100)

        addrSizer.Add(priceLbl, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 50)
        addrSizer.Add(self.price, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)

        mainSizer.Add(addrSizer, 0, wx.EXPAND|wx.ALL, 10)

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add((20,20), 1)
        btnSizer.Add(saveBtn)

        btnSizer.Add((20,20), 1)
        btnSizer.Add(cancelBtn)
        btnSizer.Add((20,20), 1)
        mainSizer.Add(btnSizer, 0, wx.RIGHT|wx.BOTTOM, 10)

        panel.SetSizer(mainSizer)
        
        self.Bind(wx.EVT_BUTTON, self.OnCloseWindow, id=ID_CLOSE)
        self.Bind(wx.EVT_BUTTON, self.OnEditProduct, id=ID_EDIT_PROD)
        self.Bind(wx.EVT_COMBOBOX, self.onProductChange)

        
    def OnCloseWindow(self, evt):
        self.Close()
    
    def OnEditProduct(self, evt):
        if self.product.GetCurrentSelection() != -1:
            try:
                price = self.price.GetValue()
                price2 = decimal.Decimal(price, 2)
                productId = self.mapeo[self.product.GetCurrentSelection()][0]
                
                data = [price2, str(productId)]
                Tickets().updateTicket(data)
                self.OnCloseWindow(evt)
            except:
                dlg = wx.MessageDialog(None, 'El precio no es correcto', 'Error', wx.OK | wx.ICON_ERROR)
                dlg.ShowModal()
        else:
            dlg = wx.MessageDialog(None, 'Debe seleccionar un item de la lista', 'Error', wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()             

    def onProductChange(self, evt):
        self.price.SetValue(str(saleDetails().getUnitPrice(self.product.GetCurrentSelection())))



